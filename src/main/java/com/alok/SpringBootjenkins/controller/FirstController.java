package com.alok.SpringBootjenkins.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {
	
	@GetMapping(path = "/")
	public String index() {
		return "Hello From Index page of SpringBoot-Jenkins v0.1 ";
	}
	
	
	@GetMapping(path = "/hello")
	public String sayHello() {
		return "Hello From SpringBoot-Jenkins v0.1 ";
	}
}

